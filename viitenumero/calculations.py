from itertools import cycle


def viite(viite):
   "return original ref with check digit added"
   return viite + tarkiste(viite)


def tarkiste(viite):
   "given a ref string, return a full reference with the check digit suffix"

   # clean away spaces and '-' chars
   viite = viite.replace('-','').replace(' ','')

   # make sure it's proper ref now
   assert(viite.isdigit())

   # make into integer digits
   digits = [int(k) for k in viite]

   # rule: go through the reference digits, starting from the end
   digits.reverse()

   # rule: multiply digits with 7,3,1 seq, so we generate pairs for that
   pairs = zip(digits, cycle((7,3,1)))

   # rule: sum the multiplications
   total = sum(a*b for a,b in pairs)

   # rule: result is zero for totals in full tens, next full tens - total for others

   # first, split out the last digit. no math needed.
   last = int(str(total)[-1])

   # then, determine the next tens
   nexttens = total + 10 - last

   # finally return subtraction result, or 0 if it's ten. again, no math needed
   return str(nexttens - total)[-1]

   
